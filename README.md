# SmallReminders
node.js backend to keep track and send messages

Phase I:
   1. Set up environment (servers, DB, testing, linting)
   1. Configure DB table to store messages
   1. Random selection and scheduling of tasks
   1. Sending messages

Phase II:
   1. Users
   1. Verification of phone number
   1. UI to display/edit
